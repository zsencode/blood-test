import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Standard from '@/components/Standard'
import Disease from '@/components/Disease'
import Solution from '@/components/Solution'
import Contact from '@/components/Contact'
import Questions from '@/components/Questions'
import Resource from '@/components/Resource'
import About from '@/components/About'
import Report from '@/components/Report'
import ReportDetails from '@/components/ReportDetails'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/standard',
      name: 'Standard',
      component: Standard
    },
    {
      path: '/disease',
      name: 'Disease',
      component: Disease
    },
    {
      path: '/solution',
      name: 'Solution',
      component: Solution
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/questions',
      name: 'Questions',
      component: Questions
    },
    {
      path: '/resource',
      name: 'Resource',
      component: Resource
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/report/:id',
      name: 'Report',
      component: Report
    }, {
      path: '/reportdetails',
      name: 'ReportDetails',
      component: ReportDetails
    }
  ]
})
