import Vuex from '../../node_modules/vuex/dist/vuex.js'
import Vue from 'vue'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    setting: {type: '0', name: '', age: ''},
    summary: {},
    top: {brand: '生化检查',
      nav_items: [{content: '首页', link: '/'}, {content: '正常值', link: '/standard'}, {content: '相关疾病', link: '/disease'}, {content: '解决方案', link: '/solution'}, {content: '联系我们', link: '/contact'}]
    },

    disease: {
      title: '生化检查临床意义',
      captions: ['项目名称', '英文对照', '临床意义', '分类'],
      data: [{cn: '谷丙转氨酶', en: 'GPT', meaning: '升高:常见于急慢性肝炎,药物性肝损伤,脂肪肝,肝硬化,心肌梗塞,心肌炎及胆道疾病等', category: '肝功能'}, {cn: '谷草转氨酶', en: 'GOT', meaning: '升高:常见于心肌梗塞发病期,急慢性肝炎,中毒性肝炎,心功能不全,皮肌炎等', category: '肝功能'}]
    },
    solution: {title: '常见疾病解决方案',
      captions: ['基本套餐', '普通套餐', '高效套餐', '可选项目'],
      data: [{disease: '骨膜损伤', regular: '硫酸亚铁盐', efficient: '硫酸亚铁盐+二甲基砜+软骨素', optional: '钙镁维生素D3复合片'}, {disease: '高血糖', regular: '蜂胶', efficient: '蜂胶+硫辛酸+肌醇', optional: '鱼油+辅酶Q10'}]
    },
    contact: {title: '联系我们', tel: '手机号', content: '咨询内容'},
    questions: { title: '常见问题',
      data: [
        {q: '你们的商品保真吗?', a: '所有商品均非市面上常见品牌,在加拿大家喻户晓但在国内鲜为人知,因为在国内销量小,制假成本高,所以这些品牌并无假货'},
        {q: '商品的卖家是谁?', a: '我们并不制造也不销售,仅就亲身经历提供有价值建议'}
      ]
    },
    resource: {title: '相关资源',
      data: [
        {title: '补硒的作用', link: 'https://baike.baidu.com/item/%E8%A1%A5%E7%A1%92'},
        {title: '补钙的作用', link: 'https://baike.baidu.com/item/%E8%A1%A5%E9%92%99'}
      ]
    },
    about: { title: '关于我们',
      data: ['本公司致力于以膳食补充剂解决绝大多数日常健康问题']
    },
    bottom: {links: [{title: '©' + new Date().getFullYear(), link: ''}, {title: '常见问题', link: '/questions'}, {title: '相关资源', link: '/resource'}, {title: '关于我们', link: '/about'}]
    },
    index: {
      title: '生化全套检查',
      body: '生化全套检查就是指用生物或化学的方法来对人进行身体检查,生化全套检查内容包括：肝功能（总蛋白,白蛋白,球蛋白,白球比,总胆红素,直接,间接胆红素,转氨酶）；血脂（总胆固醇,甘油三酯,高,低密度脂蛋白,载脂蛋白）；空腹血糖；肾功能（肌酐,尿素氮）；尿酸；乳酸脱氢酶；肌酸肌酶等',
      // section: [{title: '基本报告', class: 'bronze', bodyList: ['用户体检项目', '正常项目范围', '异常项目提醒'], link: '/report/0'}, {title: '正常报告', class: 'silver', bodyList: ['含基本报告', '异常项目危害', '异常可能诱因'], link: 'report/1'}, {title: '详细报告', class: 'gold', bodyList: ['含正常报告', '异常治疗建议', '治疗解决方案'], link: 'report/2'}]
      section: [{title: '基本报告', class: 'bronze', bodyList: ['用户体检项目', '正常项目范围', '异常项目提醒'], link: '/report/0'}, {title: '正常报告', class: 'silver', bodyList: ['含基本报告', '异常项目危害', '异常可能诱因'], link: 'report/1'}]
    },
    report: {
      // title: ['基本报告', '正常报告', '详细报告'],
      // description: ['仅限数据', '数据+病症', '+数据+病症+解决方案'],
      // section: [{title: '基本报告', class: 'bronze', bodyList: ['用户体检项目', '正常项目范围', '异常项目提醒'], link: '/report/0'}, {title: '正常报告', class: 'silver', bodyList: ['含基本报告', '异常项目危害', '异常可能诱因'], link: 'report/1'}, {title: '详细报告', class: 'gold', bodyList: ['含正常报告', '异常治疗建议', '治疗解决方案'], link: 'report/2'}]
      title: ['基本报告', '正常报告'],
      description: ['仅限数据', '数据+病症'],
      section: [{title: '基本报告', class: 'bronze', bodyList: ['用户体检项目', '正常项目范围', '异常项目提醒'], link: '/report/0'}, {title: '正常报告', class: 'silver', bodyList: ['含基本报告', '异常项目危害', '异常可能诱因'], link: 'report/1'}]
    },
    reportDetails: {
      standardDetails: {
        title: '生化检查正常值',
        captions: ['中文对照', '英文对照', '正常值范围']
      },
      diseaseDetails: {
        title: '生化检查临床意义',
        captions: ['项目名称', '英文对照', '高临床意义', '低临床意义', '分类']
      },
      data: [
        {
          title: '肝功能',
          details: [
            {name: '谷丙转氨酶', id: 'gbzam', value: '0-40U/L', en: 'ALT(GPT)', high: '常见于急慢性肝炎,药物性肝损害,脂肪肝,肝硬化,心肌梗塞,心肌炎及胆道疾病等。'},
            {name: '谷草转氨酶', id: 'gczam', value: '5-35U/L', en: 'AST(GOT)', high: '常见于心肌梗塞发病期,急慢性肝炎,中毒性肝炎,心功能不全,皮肌炎等。'},
            {name: '碱性磷酸酶', id: 'jxlsm', value: '42-128U/L', en: 'ALP(AKP)', high: '常见于肝癌,肝硬化,阻塞性黄疸,急慢性黄疸型肝炎,骨细胞瘤,骨转移癌,骨折恢复期。另外,少年儿童在生长发育期骨胳系统活跃,可使ALP增高。注意：使用不同绶冲液,结果可出现明显差异。'},
            {name: 'γ-谷氨酰转肽酶', id: 'gaxztm', value: '男8-60 女4-51U/L', en: 'GGT(γ-GT)', high: '常见于原发性或转移性肝癌,急性肝炎,慢性肝炎活动期肝硬化,急性胰腺炎及心力衰竭等。'},
            {name: '总胆红素', id: 'zdhs', value: '0.2-1.6mg/dL', en: 'TBIL', high: '肝脏疾病,肝外疾病,原发性胆汁性肝硬化 溶血性黄疸急性黄疸性肝炎 新生儿黄疸 慢性活动期肝炎 闭塞性黄疸病毒性肝炎 胆石症 阻塞性黄疸胰头癌肝硬化输血错误'},
            {name: '直接胆红素', id: 'zjdhs', value: '0-0.4mg/dL', en: 'DBIL', high: '常见于阻塞性黄疸,肝癌,胰头癌,胆石症等。'},
            {name: '总蛋白', id: 'zdb', value: '6.4-8.4g/dL', en: 'TP', high: '常见于高度脱水症（如腹泻,呕吐,休克,高热）及多发性骨髓瘤。', low: '常见于恶性肿瘤,重症结核,营养及吸收障碍,肝硬化,肾病综合征,溃疡性结肠炎,烧伤,失血等。'},
            {name: '白蛋白', id: 'bdb', value: '3.7-5.3g/dL', en: 'ALB', high: '常见于严重失水导致血浆浓缩,使白蛋白浓度上升。', low: '基本与总蛋白相同,特别是肝脏病,肾脏疾病更为明显。'},
            {name: '胆碱酯酶', id: 'djzm', value: '比色法130-310U/L 酶法:儿童和成人男性,女性（40岁以上）5410-32000U/L,女性（16-39岁）4300-11500U/L', en: 'CHE', low: '血清CHE由肝脏合成,临床上主要用于估计肝脏疾病的严重程度。如：亚急性重型肝炎患者特别是肝昏迷病人,血清CHE明显降低,且多呈持久性降低。'},
            {name: '总胆汁酸', id: 'zdzs', value: '0.1-10.0μmol/L', en: 'TBA', high: '胆汁酸在肝细胞内由胆固醇转化而成。血清胆汁酸水平是反映肝实质损伤的一个重要指标。在胆管阻塞和胆汁性肝硬化,新生儿胆汁淤积和妊娠性胆汁淤积时,TBA均有显著升高。'},
            {name: '谷氨酸脱氢酶', id: 'gastqm', value: '0-1.5U/L', en: 'GLDH', high: '急性病毒性肝炎,慢性肝炎,肝硬化。'},
            {name: '异柠檬酸脱氢酶', id: 'ysmstqm', value: '比色法238-686U/L,酶速率法(37℃)1-7U/L,紫外法1.5-7.0U/L', en: 'ICDH', high: '临床上对诊断肝病有一定意义,尤其是恶性肿瘤病人血清异枸橼酸脱氢酶升高,往往是肝脏转移的信号。升高常见于急性肝炎,慢性肝炎,肝硬化,肝癌,肝转移癌,胆石症,胆囊炎,胆道阻塞,胰腺炎,右心功能不全,肺梗死,新生儿黄疸,溶血性疾病等。'},
            {name: '亮氨酸氨基肽酶', id: 'lasajtm', value: '148-250mg/dL,氧化酶检测法(1.9-3.2mmol/L)', en: 'LAP', high: '是一种蛋白酶,肝内含量很丰富。肝内外胆淤时,LAP活力显著增高,尤其在恶性胆淤时,其活力随病情进展而持续增高。试剂对肝道梗阻及胰腺癌的诊断有价值。肝坏疽, 肝肿瘤 ,肝炎,乳腺癌, 肝癌, 胆道癌 ,胰腺癌,子宫内膜 癌,卵巢癌明显增高。'},
            {name: '前白蛋白', id: 'qbdb', value: '1岁左右的儿童前白蛋白正常值是100mg/L,1-3岁的孩子前白蛋白的正常范围是168-281mg/L,成年人则在280-360mg/L', en: 'PA', low: '诊断和监测营养不良,诊断肝病,诊断急性相反应。', high: '肾病综合症>500mg/L(此时ALB<30g/L)。'},
            {name: 'α-L-岩藻糖苷酶', id: 'yztgm', value: 'ELISA法和分光光度连续监测法为234-414μmol/L,正常值5.31-8.29u/g', en: 'AFU', low: '见于遗传性α-L-岩藻糖苷酶缺乏引起的岩藻糖积蓄病。', high: '见于原发性肝癌,转移性肝癌,肝硬化,急性肝炎等。尤其对肝细胞癌的早期诊断价值值得重视。'},
            {name: '腺苷脱氨酶', id: 'xgtam', value: '1-25U', en: 'ADA', high: '在肝脏疾病中,ADA的活性升高。与ALT或GGT等组成肝酶谱能较全面地反映肝脏疾病的酶学改变。'}
          ]
        },
        {
          title: '心肌酶',
          details: [
            {name: '乳酸脱氢酶', id: 'rutqm', value: '95-213U/L', en: 'LDH', high: '急性心肌炎发作后来2－48小时开始升高,2－4天可达高峰8－9天恢复正常。另外,肝脏疾病,恶性肿瘤可引起LDH增高。'},
            {name: '肌酸激酶', id: 'jsjm', value: '18-198U/L', en: 'CK', high: '心肌梗塞4－6小时开始升高,18－36小时可达正常值的20－30倍,为最高峰,2－4天恢复正常。另外,病毒性心肌炎,皮肌炎,肌肉损伤,肌营养不良,心包炎,脑血管意外及心脏手术等都可以使CK增高。'},
            {name: '肌酸激酶同工酶', id: 'jsjmtgm', value: '电泳法占肌酸激酶CK-MB<0.05(CK-MB<5%) CK-MM>0.94-0.96(CK-MM>94%-96%) CK-BB无或痕量(CK-BB无或痕量),酶速率法(37℃) CK-MB:0-18U/L CK-MM:0-18U/L CK-BB:0U/L', en: 'CK-MB', high: 'CK－MB主要存在于心肌中,约为心肌总CK的14%,血清CK-MB上升先于总活力的升高,24小时达峰值,36小时内其波动曲线与总活力相平行,至48小时消失。'},
            {name: 'α-羟丁酸脱氢酶', id: 'qdstqm', value: '90-182U/L', en: 'α-HBDH', high: '与LDH大致相同,在急性心肌梗塞时此酶在血液中维持高值可达到2倍左右。'}
          ]
        },
        {
          title: '肾功能',
          details: [
            {name: '尿素', id: 'ns', value: '7-20mg/dL', en: 'BUN/UREA', high: '大致可分为三个阶段。浓度在8.2-17.9mmol/L时,常见于UREA产生过剩（如高蛋白饮食,糖尿病,重症肝病,高热等）,或UREA排泻障碍（如轻度肾功能低下,高血压,痛风,多发性骨髓瘤,尿路闭塞,术后乏尿等）。浓度在17.9-35.7mmol/L时,常见于尿毒症前期,肝硬化,膀胱肿瘤等。浓度在35.7mmol/L以上,常见于严重肾功能衰竭,尿毒症。'},
            {name: '肌酐', id: 'jg', value: '酶法(男性:44-133μmol/L女性:70-106μmol/L)', en: 'CRE-ENZYME', high: '常见于严重肾功能不全,各种肾障碍,肢端肥大症等。', low: '常见于肌肉量减少（如营养不良,高龄者）,多尿。'},
            {name: '尿酸', id: 'ns', value: '男:2.5-7.2 女:1.8-6.2μmol/L', en: 'UA', high: '常见于痛风,子痫,白血病,红细胞增多症,多发性骨髓瘤,急慢性肾小球肾炎,重症肝病,铅及氯仿中毒等。', low: '常见于恶性贫血,乳糜泻及肾上腺皮质激素等药物治疗后。'},
            {name: '胱抑素C', id: 'gysc', value: '0.51-1.09mg/L', en: 'Cys', high: '胱抑素C是一种理想的反映肾小球滤过功能（GFR）变化的内源性标志物。 当肾功能受损时,肾小球滤过率下降,Cys C在血液中浓度可增加10多倍；若肾小球滤过率正常,而肾小管功能失常时,会阻碍Cys C在肾小管吸收并迅速分解,使尿中的浓度增加100多倍。'}
          ]
        },
        {
          title: '血糖',
          details: [
            {name: '血葡萄糖', id: 'xptt', value: '成人4.2-6.0mmoL/L,儿童3.3-5.5mmoL/L', en: 'GLU', high: '某些生理因素（如情绪紧张,饭后1－2小时）及静注射肾上腺素后可引起血糖增高。病理性增高常见于各种粮糖尿病,慢性胰腺炎,心肌梗塞,肢端巨大症,某些内分泌疾病,如甲状腺机能亢进,垂体前叶嗜酸性细胞腺瘤,垂体前叶嗜碱性细胞机能亢进症,肾上腺机能亢进症等。颅内出血,颅外伤等也引起血糖增高。', low: '糖代谢异常,胰岛细胞瘤,胰腺瘤,严重肝病,新生儿低血糖症,妊娠,哺乳等都可造成低血糖。'},
            {name: '糖化血红蛋白', id: 'thxhdb', value: '4%-6%', en: 'HbA1c', high: '若糖化血红蛋白>9%说明患者持续存在高血糖,会发生糖尿病肾病,动脉硬化,白内障等并发症,同时也是心肌梗死,脑卒中死亡的一个高危因素。'},
            {name: '果糖胺', id: 'gta', value: '1.5-2.4mmol/L', en: '-', high: '果糖胺是血浆中的蛋白质与葡萄糖非酶糖化过程中形成的高分子酮胺结构类似果糖胺的物质,它的浓度与血糖水平成正相关,并相对保持稳定。它的测定却不受血糖的影响。由于 血浆蛋白 的半衰期为17～20天,故果糖胺可以反映 糖尿病患者 检测前1～3周内的平均血糖水平'}
          ]
        },
        {
          title: '血脂',
          details: [
            {name: '总胆固醇', id: 'zdgc', value: '3-5.69mmol/L', en: 'CHO', high: '在5.17-6.47mmol/L时,为动脉粥样硬化危险边缘；6.47-7.76mmol/L为动脉粥样硬化危险水平；>7.76mmol/L为动脉粥样硬化高度危险水平', low: '低胆固醇血症'},
            {name: '甘油三酯', id: 'gysz', value: '0.45-1.69mmol/L', en: 'TG', high: '可以由遗传,饮食因素或继发于某些疾病,如糖尿病,肾病等。TG值2.26mmol/L以上为增多；5.65mmol/L以上为严重高TG血症。', low: '常见于甲亢,肾上腺皮质功能低下,肝实质性病变,原发 性B脂蛋白缺乏及吸收不良。'},
            {name: '高密度脂蛋白', id: 'gmdzdb', value: '男30-70mg/dL,女31-79mg/dL', en: 'HDL-C', high: '原发性高HDL血症,胰岛素,雌激素,运动,饮酒等。', low: '常见于高脂蛋白血症,脑梗塞,冠状动脉硬化症,慢性肾功能不全,肝硬化,糖尿病,肥胖等。'},
            {name: '低密度脂蛋白', id: 'dmdzdb', value: '105-130mg/dL', en: 'LDL-C', high: '见于高脂蛋白血症,急性心肌梗死,冠心病,肾病综合征,慢性肾功能衰竭,肝病和糖尿病等,也可见于神经性厌食及怀孕妇女。', low: '见于营养不良,慢性贫血,骨髓瘤,创伤和严重肝病等。'},
            {name: '脂蛋白a', id: 'zdba', value: '小于140mg/dL或小于3.5mmol/L', en: 'Lp(a)', high: '见于动脉粥样硬化性 心脑血管病 ,急性心肌梗死,家族性 高胆固醇血症 ,糖尿病,大 动脉瘤 及某些癌症等。', low: '见于肝脏疾病,酗酒,摄入新霉素等药物后。'},
            {name: '载脂蛋白A', id: 'zzdba', value: '1型1.0-1.6g/L,2型0.35-0.5g/L,4型0.13-0.16 g/L', en: 'APOA-1', high: '服用一些抗癫痫药物,长时间过量饮酒,肝脏出现异常,患有慢性肝炎,妊娠。', low: '肝脏功能受损,常见于患有动脉粥样硬化,糖尿病,高脂蛋白血症等疾病的患者。'},
            {name: '载脂蛋白B', id: 'zzdbb', value: '男43-128mg/dL,女43-128mg/dL', en: 'APOB', high: '常见于高脂血症,冠心病及银屑病。', low: '常见于肝实质性病变。'}
          ]
        },
        {
          title: '特定蛋白',
          details: [
            {name: '免疫球蛋白A', id: 'myqdba', value: '76-390mg/dL', en: 'IgA', high: '当患变态反应性疾病, 自身免疫性疾病 ,各种感染以及 多发性骨髓瘤 等时,免疫球蛋白可异常增高。'},
            {name: '免疫球蛋白G', id: 'myqdbg', value: '7-17g/L', en: 'IgG', high: '当患变态反应性疾病, 自身免疫性疾病 ,各种感染以及 多发性骨髓瘤 等时,免疫球蛋白可异常增高。'},
            {name: '免疫球蛋白M', id: 'myqdbm', value: '0.6-2.5g/L', en: 'IgM', high: '当患变态反应性疾病, 自身免疫性疾病 ,各种感染以及 多发性骨髓瘤 等时,免疫球蛋白可异常增高。'},
            {name: '补体C3', id: 'btc3', value: '单项免疫扩散法0.8-1.2g/L,火箭免疫电泳法0.9879-1.4559g/L', en: '-', high: '急性炎症或者传染病早期,如 风湿热 急性期,心肌炎,心肌梗死,关节炎等', low: '补体合成能力下降,如慢性活动性肝炎,肝硬化,肝坏死等。补体消耗或者丢失过多,如活动性红斑狼疮,急性肾小球肾炎早期及晚期,基底膜增生型肾小球肾炎,冷 球蛋白 血症,严重类风湿关节炎,大 面积 烧伤等。补体合成原料不足,如儿童 营养不良 性疾病。先天性补体缺乏。'},
            {name: '补体C4', id: 'btc4', value: '单项免疫扩散法0.44-0.66g/L,免疫散射比浊法16-47mg/dL', en: '-', high: '急性炎症或者传染病早期,如 风湿热 急性期,心肌炎,心肌梗死,关节炎等', low: '补体合成能力下降,如慢性活动性肝炎,肝硬化,肝坏死等。补体消耗或者丢失过多,如活动性红斑狼疮,急性肾小球肾炎早期及晚期,基底膜增生型肾小球肾炎,冷 球蛋白 血症,严重类风湿关节炎,大 面积 烧伤等。补体合成原料不足,如儿童 营养不良 性疾病。先天性补体缺乏。'},
            {name: 'C-反应蛋白', id: 'cfydb', value: '小于1mg/dL', en: 'CRP', high: '与冠心病相关。CRP水平与冠心病和一系列已被确认的与患心血管疾病因素相关(如纤维蛋白原,总胆固醇 ,甘油三酯,载脂蛋白B的上升,吸烟及高密度脂蛋白的下降)。与冠状动脉粥样硬化有关。是良好的预后诊断标志物。CRP的释放量与急性心梗,急性心梗引起的死亡和紧急换血管术病人的病情都有关。另外3mg/L被确认为是区分低危病人和高危病人的最佳临界值.与心肌钙蛋T(CTNT)白形成互补信息。当CRP与CTNT试验都呈阳性时,心血管危险性的预测就成为可能。'},
            {name: 'D-二聚体', id: 'dejt', value: '小于0.3mg/L或小于0.5mg/L', en: 'D-Dimer', high: '继发性纤维蛋白溶解功能亢进,如高凝状态,弥散性血管内凝血,肾脏疾病,溶栓治疗等。'}
          ]
        },
        {
          title: '离子',
          details: [
            {name: '钾', id: 'j', value: '3.4-4.7mmol/L', en: 'K', high: '：经口及静脉摄入增加。钾流入细胞外液 严重溶血及感染烧伤,组织破坏,胰岛素缺 乏。组织缺氧 心功能不全,呼吸障碍,休克。尿排泄障碍 肾功能衰竭及肾上腺皮质功能减退。毛地黄素大量服用。', low: '经口摄入减少。钾移入细胞内液 碱中毒及使用胰岛素后,IRI分泌增加。消化道钾丢失频繁呕吐腹泻。尿钾丧失 肾小管性酸中毒。'},
            {name: '钠', id: 'n', value: '135-147mmol/L', en: 'Na', high: '严重脱水,大量出汗,高烧,烧伤,糖尿病性多尿。肾上腺皮质功能亢进,原发及继发性醛固酮增多症。', low: '肾脏失钠 如肾皮质功能不全,重症肾盂肾炎,糖尿病。胃肠失钠 如胃肠道引流,呕吐及腹泻。抗利尿激素过多。'},
            {name: '氯', id: 'l', value: '98-107mmol/L', en: 'Cl', high: '常见于高钠血症,呼吸性碱中毒,高渗性脱水,肾炎少尿及尿道梗塞。', low: '常见于低钠血症,严重呕吐,腹泻,胃液胰液胆汁液大量丢失,肾功能减退及阿狄森氏病等。'},
            {name: '钙', id: 'g', value: '8.4-10.6mg/dL', en: 'Ca', high: '常见于骨肿瘤,甲状旁腺机能亢进,急性骨萎缩,肾上腺皮脂功能减退及维生素D摄入过量等。', low: '常见于维生素D缺乏,佝偻病,软骨病,小儿手足抽搐症,老年骨质疏松,甲状旁腺功能减退,慢性肾炎,尿毒症,低钙饮食及吸收不良。'},
            {name: '镁', id: 'm', value: '1.7–2.2mg/dL', en: 'Mg', high: '常见于急慢性肾功能不全,甲状腺功能低下,阿狄森氏病,多发性骨髓瘤,严重脱水及糖尿病昏迷。', low: '常见于先天家族性低镁血症,甲亢,长期腹泻,呕吐,吸收不良,糖尿病酸中毒,原发性醛固酮症,以及长期使用皮质激素治疗后。'},
            {name: '锌', id: 'x', value: '男124.3µg/g,131.2µg/g', en: 'Zn', low: '锌是前列腺功能评价指标,与抗细菌感染有关,病原微生物感染可引起前列腺炎,常表现为精液液化迟缓和精浆锌含量降'},
            {name: '铁', id: 't', value: '男61-167μg/dL,女50-150μg/dL,老人40-80μg/dL', en: 'Fe', high: '见于因红细胞大量破坏的溶血性贫血；因红细胞再生或成熟障碍而导致的再生障碍性贫血,巨幼红细胞性贫血；因铁利用率太低的铅中毒或因维生素B6缺乏引起造血功能减低等。', low: '常见于缺铁性贫血,急性或慢性感染,恶性肿瘤等。'},
            {name: '二氧化碳', id: 'eyht', value: '23-31mmol/L', en: 'CO2', high: '代谢性碱中毒：幽门梗塞（胃酸大量丢失）,小肠上部梗阻,缺钾,服碱性药物过量（或中毒）。呼吸性酸中毒：呼吸道阻塞,重症肺气肿,支气管扩张,气胸,肺气肿,肺性脑病肺实变,肺纤维化,呼吸肌麻痹,代偿性呼吸性酸中毒。高热,呼出二氧化碳过多。肾上腺皮质功能亢进,使用肾上腺皮质激素过多。', low: '代谢性酸中毒：糖尿病酮症酸中毒,肾功能衰竭,尿毒症,感染性休克,严重脱水,流行性出血热（低血压期和少尿期）,慢性肾上腺皮质功能减退,服用酸性药物过量。呼吸性碱中毒：呼吸中枢兴奋（呼吸增快,换气过度,吸入二氧化碳过多）。肾脏疾病：肾小球肾炎,肾小管性酸中毒,肾盂肾炎,肾结核。'},
            {name: '磷', id: 'l', value: '2.1-4.7mg/dL', en: 'P', high: '常见于甲状旁腺机能减退,急慢性肾功能不全,尿毒症,,骨髓瘤及骨折愈合期。', low: '常见于甲亢,代谢性酸中毒,佝偻病,肾功能衰歇,长期腹泻及吸收不良。'}
          ]
        },
        {
          title: '胰腺功能',
          details: [
            {name: '淀粉酶', id: 'dfm', value: '酶速率法(37℃)20-90U/L,碘比色血清：800-1800U/L,BMD法25-125U/L,70岁以上28-119U/L', en: 'AMY', high: '常见于急慢性胰腺炎,胰腺癌,胆道疾病,胃穿孔,肠梗阻,腮腺炎,唾液腺炎等。', low: '常见于肝脏疾病（如肝癌,肝硬化等）。'}
          ]
        }
      ]
    }
  },
  getters: {
    getTop (state) { return state.top },
    getStandard (state) { return state.standard },
    getDisease (state) { return state.disease },
    getSolution (state) { return state.solution },
    getContact (state) { return state.contact },
    getQuestions (state) { return state.questions },
    getResource (state) { return state.resource },
    getAbout (state) { return state.about },
    getBottom (state) { return state.bottom },
    getIndex (state) { return state.index },
    getReport (state) { return state.report },
    getReportDetails (state) { return state.reportDetails },
    getSummary (state) { return state.summary },
    getSetting (state) { return state.setting }
  },
  actions: {
    changeSummary ({commit}, {key, val}) {
      commit('changeSummary', {key, val})
    },
    generateReport ({commit}, {id, name, age}) {
      commit('generateReport', {id, name, age})
    },
    resetSummary ({commit}) {
      commit('resetSummary')
    }
  },
  mutations: {
    changeSummary (state, {key, val}) {
      if (val !== '2') {
        Vue.set(state.summary, key, val)
      } else {
        delete state.summary[key]
        state.summary = Object.assign({}, state.summary)
      }
      console.log(state.summary)
    },
    generateReport (state, {id, name, age}) {
      Vue.set(state.setting, 'type', id)
      Vue.set(state.setting, 'name', name)
      Vue.set(state.setting, 'age', age)
    },
    resetSummary (state) {
      state.summary = {}
    }
  }
})
